﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using tp1.Resources;
using System.Windows.Media.Imaging;

namespace tp1
{
    public partial class MainPage : PhoneApplicationPage
    {

        public Personne p;
        private Boolean animationState;

        // Constructeur
        public MainPage()
        {
            InitializeComponent();
            p = new Personne("Marcel", "Dupond", 42, true, "yolo@web.com", "Assets/Images/marcel.png");

            this.DataContext = p;

            // Permet de stocker une donnée en dehors de l'app (ou pas)
            // En fait faut utiliser des System.IO.IsolatedStorage.IsolatedStorageSettings à la place
            if (PhoneApplicationService.Current.State.ContainsKey("Prenom"))
            {
                p.Prenom = (String)PhoneApplicationService.Current.State["Prenom"];
            }

            //On définit, par code le listener de la textbox du prénom
            tb_prenom.TextInput += this.tb_prenom_TextInput;

            /*
            tb_prenom.Text = p.Prenom;
            tb_nom.Text = p.Nom;
            tb_mail.Text = p.Mail;
            tb_age.Text = p.Age.ToString();
            
            img_photo.Source = new BitmapImage(new Uri(p.Photo, UriKind.Relative));
            */


            /*
            Style s = (Style)Application.Current.Resources["MonStyle"];
            tb_age.Style = s;
            */


            // Exemple de code pour la localisation d'ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TestAnimation_Click(object sender, RoutedEventArgs e)
        {
            if (animationState)
            {
                Animation.Begin();
                animationState = false;
                bt_yolo.Content = "YOLO !";
            }
            else
            {
                Animation.Stop();
                animationState = true;
                bt_yolo.Content = "42 !";
            }

            PhoneApplicationService.Current.State["Prenom"] = p.Prenom;

        }

        private void tb_prenom_TextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            p.Prenom = e.Text;
        }

        // Exemple de code pour la conception d'une ApplicationBar localisée
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Définit l'ApplicationBar de la page sur une nouvelle instance d'ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Crée un bouton et définit la valeur du texte sur la chaîne localisée issue d'AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Crée un nouvel élément de menu avec la chaîne localisée d'AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}