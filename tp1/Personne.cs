﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace tp1
{
    public class Personne : INotifyPropertyChanged
    {
        private UInt16 age;
        public UInt16 Age
        {
            get { return age; }
            set { if( value >= 0 ) age = value; }
        }

        private String nom;
        public String Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        private String prenom;
        public String Prenom
        {
            get { return prenom; }
            set {
                if (prenom != value)
                {
                    prenom = value;
                    notifyChanged();
                }
            }
        }

        private String mail;
        public String Mail
        {
            get { return mail; }
            set { mail = value; }
        }

        private Boolean sexe;
        public Boolean Sexe
        {
            get { return sexe; }
            set { sexe = value; }
        }

        private String photo;
        public String Photo
        {
            get { return photo; }
            set { photo = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void notifyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public Personne()
        {

        }

        public Personne(String prenom, String nom, UInt16 age, Boolean sexe, String mail, String photo)
        {
            this.Prenom = prenom;
            this.Nom = nom;
            this.Age = age;
            this.Sexe = sexe;
            this.Mail = mail;
            this.Photo = photo;
        }


    }
}
